import React from 'react'
import { ScrollView, View, FlatList, Text} from 'react-native'

const alunos = [
    {id: 1, nome: 'Joao', nota:7.9 },
    {id: 2, nome: 'Pedro', nota:8.9 },
    {id: 3, nome: 'Tiago', nota:4.6 },
    {id: 4, nome: 'Andressa', nota:5.6 },
    {id: 5, nome: 'Ana', nota:7.8 },
    {id: 6, nome: 'Rafael', nota:6.6 },
    {id: 7, nome: 'Diogo', nota:4.2 },
    {id: 8, nome: 'Elisa', nota:9.9 },
    {id: 9, nome: 'Lucas', nota:10.0 },

    {id: 11, nome: 'Joao', nota:7.9 },
    {id: 12, nome: 'Pedro', nota:8.9 },
    {id: 13, nome: 'Tiago', nota:4.6 },
    {id: 14, nome: 'Andressa', nota:5.6 },
    {id: 15, nome: 'Ana', nota:7.8 },
    {id: 16, nome: 'Rafael', nota:6.6 },
    {id: 17, nome: 'Diogo', nota:4.2 },
    {id: 18, nome: 'Elisa', nota:9.9 },
    {id: 19, nome: 'Lucas', nota:10.0 },

]

const ItemEstilo = {
    paddingHorizontal: 15,
    height: 50,
    backgroundColor: '#DDD',
    borderWidth: 0.5,
    borderColor: '#222',

    // Flex
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between'
}

export const Aluno = props =>
    <View style={ItemEstilo}>
        <Text>Nome: {props.nome}</Text>
        <Text style={{ fontWeight:'bold'}}>Nota: {props.nota}</Text>
    </View>

export default props => {
    const renderItem = ({ item }) => {
        return <Aluno {...item} />
    }   

return (
    <ScrollView>
        <FlatList data={alunos} renderItem={renderItem}
            keyExtractor={(_, index) => index.toString()} />
    </ScrollView>
 )
}
