/** @format */

import {AppRegistry} from 'react-native';
// * import App from './android/src/App';
import {name as appName} from './app.json';
import Menu from './android/src/Menu'

AppRegistry.registerComponent(appName, () => Menu);
